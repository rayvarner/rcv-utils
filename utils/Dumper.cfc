/**
 * Dumper: Useful debugging tool in tight spaces
 * 
 *      When debugging code there are some circumstances where dumping a var is difficult,
 *          writing to a log is not enough and setting up step-debugging is not convenient. 
 * 
 *      Ex: in the onRequestStart() of Application.cfc
 *          request.Dumper = new rcv.utils.Dumper();
 *      Somewhere that is difficult to debug:
 *              
 *              // assuming         writePath = d:\wwwroot
 *                                  returnUrlPrefix = 'http://localhost/'
 * 
 *          UrlToDumpFile = request.Dumper.dumpMe({args:arguments, time:now(), variableKeys:structKeyList(variables)});
 * 
 *          UrlToDumpFile
 *                  http://localhost:80/_dumper_19991231_235959.htm
 * 
 *          Dumper wrote a cfdump to 
 *                  d:\wwwroot\_dumper_19991231_235959.htm
 * 
 * =============================================================================================
 *    REST/JavaScript Example:
 *      
 *      - in (CF) REST component:
 *          try{}
 *          catch(any e) {
                var response={ 
                    status=500,                                         // status 500 is an application error
                    content="#serializeJSON(e)#",                       // returning the CF error as JSON in the body
                    headers={ 
                        'Content-Type': 'application/json'
                        , 'X-Custom-Header': 'RestAppError'             // the JavaScript can look for this 'RestAppError' value
                        , 'Content-Location': request.Dumper.dumpMe(e)  //The Content-Location header indicates an alternate location for the returned data.
                        } 
                    };
                restSetResponse(response);
            }

        - in JavaScript
            try {
                if(response.headers.get('Content-Location').length) {
                    let myErrWin = window.open(response.headers.get('Content-Location'));
                }
            }
            catch(err) {
                console.error(err);
            }

        Therefore, when JavaScript recieves the 'Content-Location' from a REST call, 
            a new browser window will open to 'http://localhost:80/_dumper_19991231_235959.htm' ... the value returned by request.Dumper.dumpMe(e)
 * 
 */
component accessors=true {

    property name="writePath" default = "#expandPath('/')#";
    property name="returnUrlFromDumpMe" default="true";
    property name="returnUrlPrefix" default="#cgi.server_port_secure?'https':'http'#://#cgi.SERVER_NAME#:#cgi.SERVER_PORT#/";
    param name="thisCfcName" default="#getComponentMetaData(THIS).name#";

    public rcv.utils.Dumper function init(
        string writePath = "#expandPath('/')#"       // canonical location of where to write the file ex: D:\wwwroot\
        , boolean returnUrlFromDumpMe = true
        , string returnUrlPrefix = "#cgi.server_port_secure?'https':'http'#://#cgi.SERVER_NAME#:#cgi.SERVER_PORT#/" // ex: http://localhost:80/
    ) {
        structAppend(variables, arguments, true);
        return this;
    }


/**
 * I write a cfdump type variable to a file
 * If variables.returnUrlFromDumpMe, I return the URL to the file
 *
 * @theVal 
 */
    public string function dumpMe(
        required any theVal
    ) {
        var returnUrl='';
        try {
            var tmpFileName = '_dumper_' & dateFormat(now(), 'yyyymmdd') & '_' & timeFormat(now(), 'HHmmss') & '.htm';

            lock type="exclusive" name="Dumper_#Hash(GetCurrentTemplatePath())#" timeout="500" throwontimeout="true" {
                writeDump(
                    var=arguments.theVal
                    , format='html'
                    , output=(variables.writePath & tmpFileName)
                    );
            }

            if(variables.returnUrlFromDumpMe) {
                returnUrl = variables.returnUrlPrefix & tmpFileName;
            }
        }
        catch(any e) {
            writeLog(text="== #variables.thisCfcName# Problem writing dumper file: #e.message# #e.detail#", type="FATAL");
        }
        return returnUrl;
    }


}