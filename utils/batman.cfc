component  hint="Im Batman. I have a utility belt full of gadgets."
{
	public rcv.utils.batman function init() {
		return this;
	}

/*
	TODO: update the instructions/requirements

	Query your database.
	order the recordset by an ID
	convertQueryForJSONOutput(q,ID)
		will convert the record set into a struct with arrays for the "manys" in the query result

	ex: ID, MOVIE, ACTR, CATEGORY
		1000,	StarWars, 	HarrisonFord,	SciFi
		1000,	StarWars, 	CarrieFisher,	SciFi
		1000,	StarWars, 	HarrisonFord,	Adventure
		1000,	StarWars, 	CarrieFisher,	Adventure
		1001,	Grease, 	OliviaNewtonJohn,	Musical
		1001,	Grease, 	JohnTravolta,	Musical

	ouputs:
		{
			'1000': {'MOVIE':'StarWars', 'ACTR':['HarrisonFord','CarrieFisher'], 'CATEGORY':['SciFi','Adventure'] }
			, '1001': {'MOVIE':'Grease', 'ACTR':['OliviaNewtonJohn','JohnTravolta'], 'CATEGORY':'Musical' }

				================  NOTE that the value for Category is a string for '1001' =======================
		}


	, array colsThatShouldBeAnArray // [{
										'col':'catKeyVal'
										, 'key': 'id_category' 
										, 'delim':'|' //-------------------------- default, optional
										}, {'col':'actrKeyVal', 'key':'id_actr'}
										] ... if the value is a 2-item pipe delimited list, it becomes a key/value pair
									// ex: ACTR ... 1001|JohnTravolta becomes   rsltStruct.ACTR = [{'id_actr':'JohnTravolta'}]

	colsThatShouldBeAnArray : 
	[ {'col':'actr',  'key':'id_actr' } // 	'delim':'|' optional default 
	, {'col':'category', 	'key':'id_cat', 	'delim':'_'} ]
*/
	public any function convertQueryForJSONOutput(
		required query q
		, required string rowKey
		, array colsThatShouldBeAnArray = []
		, boolean returnAsArray = true // if true, the struct to be returned is converted to an array
		) {
		var colsThatShouldBeAnArray = arguments.colsThatShouldBeAnArray;
		var reformatDataType = 0;
		var reformatData = 0;
		var convertStringToStruct = 0;

		var convertStructToArray = 0;
		if(arguments.returnAsArray) {
			convertStructToArray = function(required struct outStruct) {
				var outArray = outStruct.reduce(function(rslt, key, val) {
					//structInsert(val, '_id', key);
					arrayAppend(rslt, val);
					return rslt;
				}
				, []);
				return outArray;
			}
		}


		// if the colsThatShouldBeAnArray value was passed, check the data within and assign closures 
		if(arrayLen(colsThatShouldBeAnArray)) {
			colsThatShouldBeAnArray.each(function(itm, indx, origArr) {
				if( 
					!structKeyExists(itm, 'col') || !len(itm['col']) 
					|| !structKeyExists(itm, 'key') || !len(itm['key']) 
					) {
						throw('Required keys do not exist in optional argument: colsThatShouldBeAnArray[{col:val, key:val [,delim:|] }, ...]', 'custom');	
					}
				});
			convertStringToStruct = function(required string data, required string key, string delim='|') {
				if(findNoCase(delim, data)) {
					var tArray = listToArray(data,delim);
					var s={};
					structInsert(s, key, arrayFirst(tArray));
					structInsert(s, 'val', arrayLast(tArray));
					return s;
				}
				else {
					return uCase(data);
				}
			}
			reformatDataType = function(required struct currRowStr, function convertStringToStruct) {
				var defaultDelim='|';
				colsThatShouldBeAnArray.each(function(itm, indx) {
					if(isSimpleValue( currRowStr[itm.col] ) && len(currRowStr[itm.col]) ) {
						currRowStr[itm.col] = [ convertStringToStruct( currRowStr[itm.col], itm.key, structKeyExists(itm, 'delim') ? itm.delim : defaultDelim ) ];
					}
					else {
						currRowStr[itm.col] = [];
					}
				});
				return currRowStr;
			}
		}
		
		var rowID = arguments.rowKey;
		var outStruct = arguments.q.reduce(
			function(rsltStruct, currentRowStruct, i, qReference) {
				if(!structIsEmpty(rsltStruct)) {
					var currentRowID = currentRowStruct[rowID];
					if( structKeyExists(rsltStruct, currentRowID) ) {
						// create a temp variable for the matched key/val
						var existingEntry = rsltStruct[currentRowID];
						// remove the rowKey from the currentRowStruct for simplicity
						structDelete(currentRowStruct, rowID);
						currentRowStruct = (isClosure(reformatDataType)) ? reformatDataType(currentRowStruct, convertStringToStruct) : currentRowStruct;
						// LOOP over the 'columns' for this 'row' of the query
						currentRowStruct.each(function(columnName,rowCellData){
							if( 
								isArray(existingEntry[columnName])
								&&
								!existingEntry[columnName].some(function(arrVal, arrIndx) {
									rowCellData = isArray(rowCellData)?rowCellData[1]:rowCellData;
									return arrVal['val'] == rowCellData['val'];
								})
							) {
								arrayAppend(existingEntry[columnName], rowCellData);
							}
						}
						, true // execute in parallel
						);
					}
					else {
						structInsert(rsltStruct, currentRowStruct[rowID], (isClosure(reformatDataType)) ? reformatDataType(currentRowStruct, convertStringToStruct) : currentRowStruct);
					}
				}
				// this is called on the first time through the loop
				else {
					structInsert(
						rsltStruct
						, currentRowStruct[rowID]
						, (isClosure(reformatDataType)) ? reformatDataType(currentRowStruct, convertStringToStruct) : currentRowStruct
						);
				}
				// return struct to the next iteration
				return rsltStruct;
			}
			, {} // initial Value for rsltStruct
		);

		return isClosure(convertStructToArray) ? convertStructToArray(outStruct) : outStruct;

	}


/**
 * Simple server check
 */
	public boolean function isServerLucee() {
		return server.coldfusion.productname == 'Lucee';
	}

/**
 * Im used to determine if a request originates from the same server
 */
	public boolean function isServerRequestFromSame() {
		return cgi.server_name == cgi.local_host;
	}


/**
 * I return a struct sorted by its keys. 
 * 		Handy in some REST situations ... returning JSON
 */
	public struct function sortStructByKeys(required struct s) {
		var r = structNew("linked");
		var keys = structKeyArray(arguments.s).sort('textnocase','asc');
		for(var key in keys) {
			structInsert(r, key, arguments.s[key]);
		}
		return r;
	 }

/**
 * I return the string name of a file w/out the last 4 chars
 *
 * @mFile string
 */
public string function getNameWithoutExt(required string mFile) {
	return mid(arguments.mFile, 1, len(arguments.mFile)-4);
}


public string function aspectRatio(
	required numeric mWidth
	, required numeric mHeight
) {
	var lcd = variables.GCD(arguments.mWidth, arguments.mHeight);
	if(isNull(lcd) || !isNumeric(lcd)) {
		lcd = 1;
	}
	return arguments.mWidth/lcd & ':' & arguments.mHeight/lcd;
}


/**
 * Calculates the GCD (greatest common factor [divisor]) of two positive integers using the Euclidean Algorithm.
 *
 * @param int1      Positive integer.
 * @param int2      Positive integer.
 * @param showWork      Boolean.  Yes or No.  Specifies whether to display work.  Default is No.
 * @return Returns a numeric value.  Optionally outputs a string.
 * @author Shakti Shrivastava (shakti@colony1.net)
 * @version 1, November 8, 2001
 */
public any function GCD(int1, int2)
{
  Var ShowWork=False;
  If (ArrayLen(Arguments) eq 3 AND Arguments[3] eq "yes"){
    ShowWork=True;
  }

  // if both numbers are 0 return undefined
  if (int1 eq 0 and int2 eq 0) return 'Undefined';

  // if both numbers are equal or either one of them is equal to 0
  // then return any 1 of those numbers appropriately
  if (int1 eq int2 or int2 eq 0) return int1;
  if (int1 eq 0) return int2;

  // if int2 divides int1 "properly" then we have reached our final
  //   step. So we output the last step and exit the function.
  if (int1 mod int2 eq 0) {
    if(ShowWork EQ True) {
      WriteOutput("<br>"&int1&"= "&fix(int1/int2)&" X "&int2&" + "&int1 mod int2);
    }
    // this line outputs the last iteration
    if (ShowWork EQ True) {
      return "<p>GCD = "&int2; //print the GCD
    }
    else{
      return int2;
    }
  }

  // if int2 does not divides int1 "properly" then we recurse using
  // int1 equal to int2 and int2 equal to int1 mod int2
  else {
    if(ShowWork EQ True)
      // this line outputs calculations from each iteration. you can safely
      // delete/comment out this line if you dont need to display the steps.
      WriteOutput("<br>"&int1&"= "&fix(int1/int2)&" X "&int2&" + "&int1 mod int2);
  }
  //since we still have not reached the last step we recall the function
  //(recurse)
  GCD(int2, int1 mod int2, ShowWork);
}














/**
 * CreateSimpleImg()
 *		Provide text to be written to an image (content), image name (id), and where to write (writeDestination).
				THEN, in the rendered page, variables can be used.
			<img
					style="max-height:300px; background-image: url(/images/placeholder.png);"
					src="/images/I_am_the_name_of_file_to_be_created.jpg"
					id="foobar"
					onerror="this.src = ''/images/broken_image.jpg''"
				/>

ex: 
args = {
	'_id': 'I_am_the_name_of_file_to_be_created.jpg'
	, 'content': 'first line|second line|third line'
	, 'writeDestination': 'c:\wwwroot\images'
}
createSimpleImg(argumentCollection=args);

 */
	public any function createSimpleImg(
		required string _id 									// this is the name of the file
		, required string content 								// Text written to img - Pipe delimits for multi line
		, string writeDestination = expandPath('/images')		// Where to write
	) {
		var img = imageNew('', 600, 160, 'RGB', '45aaf2');

		/* STYLE
			A structure used to specify the text characteristics. the following keys are supported:
		font: 						The name of the font used to draw the text string. If you do not specify the font attribute, the text is drawn in the default system font.
		size: 						The font size for the text string. The default value is 10 points.
		style: 						The style to apply to the font ( bold,italic,boldItalic,plain (default) ).
		strikethrough: 		a boolean that specify whether strikethrough is applied to the text image, default is false.
		underline: 				a boolean that specify whether underline is applied to the text image, default is false.
		*/
		var style = {
			size:'20'
			, style: 'bold'
		};

		// pipe delimeted list of values
		var mArray = listToArray(arguments.content, '|');
		var amtYtoAdd = 0;
		var amtXtoAdd = 0;
		for(var item in mArray) {
			ImageDrawText(img, item, 10+amtXtoAdd, 35+amtYtoAdd, style);
			amtYtoAdd = amtYtoAdd + 30;
			amtXtoAdd = amtXtoAdd + 50;
		}

		//ImageDrawText(img, arguments.content, 10, 35, style);
		//cfimage(action="writeToBrowser", source=img);

		//imageScaleTofit(img,600,400,"lanczos");

		var filePathAndName = arguments.writeDestination & '\' & arguments._id ; //& '.jpg';
		lock scope="server" type="exclusive" timeout="30" {
			imageWrite(img, filePathAndName, .5, true);
		}
	}


	public struct function makeImagesUsingThreading(
		required array itemArray 						// an array of {'_id':'', 'content':'', 'writeDestination':''}
	) {
			// struct that will be returned
			var ret = {
				  threadsWithErrors: []
				, itemsSuccessfullyProcessed: 0
				, totalItems: 0
			};
	
			var threadPrefix = listFirst(createUUID(),'-');
	
			// used to join created threads
			var threadList='';
			
			// record to the return struct
			ret.totalItems = arrayLen(arguments.itemArray);
	
			// loop over the array
			for(var i=1;i<=ret.totalItems;i++) {
	
				// optional progress within the dev.log
				/*if(i mod 50 == 0) {
					writeLog(text="== user #i# of #ret.totalItems#", type="info", file="dev");
				}*/
	
				var threadKey='#threadPrefix#_#i#';
				// add the new thread name to the threadList
				threadList = listAppend(threadList, threadKey);
				
				// item to send into the thread
				var item = duplicate(arguments.itemArray[i]);
				
				thread action="run" name="#threadKey#" item="#item#" {
					
					// each thread has default variables: {ELAPSEDTIME,NAME,OUTPUT,PRIORITY,STARTTIME,STATUS,STACKTRACE}
					// add additional variables for cfdumping
	
					thread.errors = [];
					
					try {
					
						this.createSimpleImg(argumentCollection = attributes.item);				
					
					}
					catch(any e) {
						
						arrayAppend(thread.errors, '#e.message# #e.detail#');
					}
					
					
					// sleep the thread so the others can catch up
					thread  action="sleep" duration=100;// IN millisecs: 1000 milliseconds = 1 sec  
					
				} // /thread:run
			} // /for
			
			//writeLog(text="== Start thread.join ============================================", type="info", file="dev");
			
			// join the threads
			thread action="join" name="#threadlist#"; 
	
			return ret;
	}
	

/*
I create a directory path. Use with caution! ONLY TESTED WITH WINDOWS

Assume you have a "C:\Temp" directory and you need a file to exists at 
	"C:\Temp\level1\level2\level3" for "C:\Temp\level1\level2\level3\fileThatShouldExist.jpg"

Pass in a argument of "C:\Temp\level1\level2\level3" 
	and the function will create
		C:\Temp\level1, C:\Temp\level1\level2, C:\Temp\level1\level2\level3

======= simple test ===============
	s={};
	s.tPath = getTempDirectory() & 'level11' & '\' & 'level12'& '\' & 'level13';
	s.dirExists_pre = directoryExists(s.tPath);
	try {
		batman.createDirectoryPath(s.tpath);
		s.dirExists_post = directoryExists(s.tPath);
		writeDump(s); 
	}	
	catch(any e) {
		writeDump({'e':e, 's':s});
	}	
===================================
*/
public void function createDirectoryPath( 
	required string mPath
) hint="I am recursive" {
	
	if(DirectoryExists(arguments.mPath)) {
		return;
	}
	
	var dirArray = listToArray(arguments.mPath, '\');	
	var dirArrayUpLevel = arraySlice( dirArray, 1, arrayLen(dirArray)-1 );
	
	var oneLevelUp = arrayToList(dirArrayUpLevel, '\');
		
	if(!directoryExists(oneLevelUp)) {
		this.createDirectoryPath(oneLevelUp);
		lock type="exclusive" scope="Server" throwontimeout="true" timeout="90"  {			
			directoryCreate(arguments.mPath); 
		}
	}
	
}





/**
I determine if a string contains Japanese chars.
	rcv.utils.udfs (required)
*/
	public boolean function hasJapaneseChars(
		required string stringToSearch
		, rcv.utils.udfs udfs = new rcv.utils.udfs() // TODO: batman is to stand alone!
		, string myregex = "[\u3040-\u30ff\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff\uff66-\uff9f]"
	) {
			var ret = arguments.udfs.jReMatchNoCase(
				  regEx = arguments.myregex
				, string = arguments.stringToSearch
				, scope = 'ONE'
			);
			return (isArray(ret) && arrayLen(ret)==1) ? true : false;
	}

/* =============================================
* TODO: Turn this into a testable function.
* ==============================================
<!---
		 https://translate.google.com/?sl=ja&tl=en&q=
		 https://www.bing.com/translator/?from=ja&to=en&text=
		 https://translate.yandex.com/?lang=ja-en&text=
--->
	<cffunction access="public" name="getTranslationResultFromUrl" returntype="string">
		<cfargument name="searchTerm" type="string" required="true" />
		<cfargument name="searchURL" type="string" default="https://translate.google.com/?sl=ja&tl=en&q=" />


		<cfargument name="myregex" type="string" default="[\u3040-\u30ff\u3400-\u4dbf\u4e00-\u9fff\uf900-\ufaff\uff66-\uff9f]" />
		<cfscript>
			//arguments.myregex="[\p{InKatakana}|\p{InHiragana}|\p{InHanunoo}|\p{IsHan}|\p{IsHangul}]";
			var ret = this.getUdfs().jReMatchNoCase(
				  regEx = arguments.myregex
				, string = arguments.searchTerm
				, scope = 'ONE'
			);
			//return (isArray(ret) && arrayLen(ret)==1) ? 'yes, has japanese' : 'no japanese';
			if(arrayLen(ret)==0) {
				return arguments.searchTerm;
			}
		</cfscript>

		<cfset var myResult = {} />
		<cfhttp timeout="500" useragent="#variables.myUserAgent#"
			url="#arguments.searchURL##arguments.searchTerm#"
			Result="myResult"
			resolveUrl="true"
			method="get"
			redirect="true">
		 	<cfhttpparam type="header" name="Accept-Encoding" value="deflate;q=0" />
		</cfhttp>

		<Cfscript>
			var ret = [];
			ret = this.getUdfs().jReMatchNoCase(
				  regEx = '(?m)(?s)(?<=<span\stitle="#arguments.searchTerm#".{1,200}\>).*?(?=</span>)' // THIS IS ONLY WITH THE GOOG URL!!!
				, string = myResult.fileContent
				, scope = 'ONE'
			);
			return (isArray(ret) && arrayLen(ret)==1) ? ret[1] : arguments.searchTerm;
		</Cfscript>
	</cffunction>
*/


/**
I randomize the order of elements in an array.
 (Source: https://gist.github.com/stevereich/3882887)
*/
	public void function shuffleArray(
		required array mArray
	) {
		lock name="batman_shuffleArray" type="exclusive" timeout="100" {
			CreateObject( "java", "java.util.Collections" ).Shuffle(arguments.mArray);
		}
	}

/**
I produce an isValid email address (example.com, example.net, and example.org)
*/
	// http://stackoverflow.com/questions/1368163/is-there-a-standard-domain-for-testing-throwaway-email
	public string function fakeEmail() {
		var emailDomains = ['com','net','org'];
		var tmpEmail = mid(createUUID(), 1, 8) & '@example.' & emailDomains[randRange(1,3,'SHA1PRNG' )];
		if(isValid("email" , tmpEmail)) {
			return tmpEmail;
		}
		else {
			throw("Problem generating fake data in #getComponentMetaData(THIS).name#.fakeEmail()");
		}
	}

/**
I produce an isValid SSN for testing purposes.
*/
	public any function fakeSSN() {
		var tmpSsn = arrayToList(getArrayOfNumbersFromUUID(3), '') & '-' &
				arrayToList(getArrayOfNumbersFromUUID(2), '') & '-' &
				arrayToList(getArrayOfNumbersFromUUID(4), '');
		if(isValid("ssn" , tmpSsn)) {
			return tmpSsn;
		}
		else {
			throw("Problem generating fake data in #getComponentMetaData(THIS).name#.fakeSSN()");
		}
	}

/**
I produce an isValid telephone number. Ten digits with no formatting.
 - This function is recursive!
*/
	public string function fakePhone() {
		var phoneNum = //'(' &
				arrayToList(getArrayOfNumbersFromUUID(3), '') & //') ' &
				arrayToList(getArrayOfNumbersFromUUID(3), '') & //'-' &
				arrayToList(getArrayOfNumbersFromUUID(4), '');
		// if the phoneNum does not pass validation, try again.
		while(!isValid("telephone" , phoneNum)) {
			phoneNum = this.fakePhone();
		}
		return phoneNum;
	}

/**
I produce an array of numbers; 0-9.
- This function is recursive!
*/
	private array function getArrayOfNumbersFromUUID(
		numeric howMany = 0 // size of the array returned.
	) {
		var arrayOfNumbers = reMatch('\d', createUUID());
		if(arguments.howMany != 0) {
			// increase the amount of numbers if necessary
			while(arrayLen(arrayOfNumbers) < arguments.howMany) {
				arrayOfNumbers = arrayMerge(arrayOfNumbers, this.getArrayOfNumbersFromUUID());
			}
			arrayOfNumbers = arrayslice(arrayOfNumbers,1,arguments.howMany);
		}
		// optional, but verify datatype is actually Number
		for(var i=1;i<=arrayLen(arrayOfNumbers);i++) {
			arrayOfNumbers[i] = arrayOfNumbers[i]*1;
		}
		return arrayOfNumbers;
	}

/**
I produce an array of letters. All uppercase. (A-F)
- This function is recursive!
*/
	private array function getArrayOfLettersFromUUID(
		numeric howMany = 0 // size of the array returned.
	) {
		var ArrayOfLetters = reMatch('[a-zA-Z]', createUUID());
		if(arguments.howMany != 0) {
			// increase the amount of numbers if necessary
			while(arrayLen(ArrayOfLetters) < arguments.howMany) {
				ArrayOfLetters = arrayMerge(ArrayOfLetters, this.getArrayOfLettersFromUUID());
			}
			ArrayOfLetters = arrayslice(ArrayOfLetters,1,arguments.howMany);
		}
		return ArrayOfLetters;
	}

/**
I produce a date of birth in MM/DD/YYYY format based on the forWhatAge argument.
*/
	public date function fakeDOB(
		forWhatAge = 65
	) {
		var dob = '1/1/' & year(now()) - arguments.forWhatAge;

		if(arguments.forWhatAge==65) {
			// add years
			dob = dateAdd("yyyy",  randRange(1,40,'SHA1PRNG' ), dob);
		}

		// add months
		dob = dateAdd("m",  randRange(1,12,'SHA1PRNG' ), dob);
		// add days
		dob = dateAdd("d",  randRange(1,31,'SHA1PRNG' ), dob);

		// verify a good date
		while( !isDate(dob) ) {
			dob = this.fakeDOB(arguments.forWhatAge);
		}
		// if an age was passed in, be sure the age is correct
		if(arguments.forWhatAge!=65) {
			while(dateDiff('yyyy',dob, now() ) != arguments.forWhatAge) {
				dob = this.fakeDOB(arguments.forWhatAge);
			}
		}
		return dateFormat(dob, 'MM/DD/YYYY');
	}


/**
I produce a date within a plus/minus number of days from a date argument. (MM/DD/YYYY)
ex: fakeDateWithinDaysOfDate('4/1/2000', 20, true)
could return either:
03/13/2000 (Wednesday, March 22, 2000)
04/11/2000 (Tuesday, April 11, 2000)
*/
	public date function fakeDateWithinDaysOfDate(
		required date myDate
		, numeric numberOfDaysPlusMinus = 45	// 45 days before date, to 45 days after date
		, boolean dateWithinMonToFri = true
	) {

		var rslt = this.fakeDateForRange(
			dateAdd("d",  numberOfDaysPlusMinus*-1, arguments.myDate)
			, dateAdd("d",  numberOfDaysPlusMinus, arguments.myDate)
			, arguments.dateWithinMonToFri
		);

		// if the result happens to be the same date as the myDate arg, call until a difference is returned.
		while( dateDiff('d',rslt, arguments.myDate )==0 ) {
			rslt = this.fakeDateWithinDaysOfDate(argumentCollection = arguments);
		}
		return rslt;

	}

/**
The dateFrom and dateTo args are possible results:
	ex: "02/08/2016"-"02/10/2016" could return "02/08/2016" or "02/10/2016"  or "02/11/2016"
*/
	public date function fakeDateForRange(
		required date dateFrom
		, required date dateTo
		, boolean dateWithinMonToFri = true
	) {
		var numberOfDaysInRange = dateDiff("d",arguments.dateFrom,arguments.dateTo);
		// validate the range of dates provided.
		if(numberOfDaysInRange < 1) {
			throw('Bad date range provided.','custom');
		}

		var	myDate = dateFormat(dateAdd("d",  randRange(0,numberOfDaysInRange,'SHA1PRNG' ), arguments.dateFrom), 'MM/DD/YYYY');

		// prevent Sat/Sun dates.
		if(arguments.dateWithinMonToFri) {
			while(arrayContains([1,7],dayOfWeek(myDate))) {
				myDate = this.fakeDateForRange(argumentCollection = arguments);
			}
		}
		return myDate;
	}


/**
I create a string from UUID chars. All uppercase. (A-F)
*/
	public string function fakeWord(
		numeric numChars = 15 - randRange(1,10,'SHA1PRNG' ) // 'if no arg, should be between 5 and 14 chars'
	) {
		return arrayToList(getArrayOfLettersFromUUID(arguments.numChars), '');
	}

/**
I create a string that resembles a STREET address. ex: 370 ECBFCA CDEAD HWY
*/
	public string function fakeAddress() {
		var designations = ['ST', 'RD', 'LN', 'HWY', 'CIR', 'CT', 'LP'];
		var stNum = randRange(1,400,'SHA1PRNG' );
		var addr = stNum & ' ' & this.fakeWord();
		if(stNum mod 5 == 0) {
			addr &= ' ' & this.fakeWord();
		}
		else if(stNum mod 7 == 0) {
			return 'PO BOX ' & stNum;
		}
		addr &= ' ' & designations[randRange(1,arrayLen(designations),'SHA1PRNG' )];
		return addr;
	}

/**
I return a simple string of 5 to 15 characters All uppercase. (A-F)
*/
	public string function fakeCity() {
		var numChars = 15 - randRange(1,10,'SHA1PRNG' );
		return arrayToList(getArrayOfLettersFromUUID(numChars), '');
	}

/**
I return a 2 character string representing a state. Not all states are represented.
*/
	public string function fakeState() {
		var states = ["GA","TN","FL","VA","IL","OH","NV","SC","NC","NY","AL","MI","DE","KY","MA","TX"];
		return states[randRange(1,arrayLen(states),'SHA1PRNG' )];
	}

/**
I produce an isValid zipcode.
*/
	public string function fakeZipcode() {
		var zip = arrayToList(getArrayOfNumbersFromUUID(5), '');
		while(!isValid("zipcode",zip)) {
			zip = this.fakeZipcode();
		}
		return zip;
	}

// ==============================================================================================

/**
I return a new Java string builder object
		sb.append('I am text');
		return sb.toString().trim();
 */
	public any function newSB() {
		return createObject('java', 'java.lang.StringBuilder').init('');
	}

/**
I return an xml string in a pretty print format.
*/
public string function prettyPrintXML(
	required xml myXml
	) {
	var sb = this.newSB();
	var crlf = chr(10); //&chr(13);
	var out = replace(arguments.myXml, '>', '>' & crlf, 'ALL');
	var out = replace(out, '<', crlf & '<', 'ALL');
	var rows = listToArray(out, crlf);
	//return rows;
	var stack = 0;
	for (var i = 1; i <= arrayLen(rows); i++) {
		var row = trim(rows[i]);
		if(len(row)) {
			if(row.startsWith("<?")) {
				// xml version tag
				sb.append(row & crlf);
			}
			else if(row.startsWith("</")) {
				// closing tag
				var indent = repeatString("    ", --stack);
				sb.append(indent & row & crlf);
			}
			else if(row.startsWith("<")) {
				// starting tag
				var indent = repeatString("    ", stack++);
				sb.append(indent & row & crlf);
			}
			else {
				// tag data
				var indent = repeatString("    ", stack);
				sb.append(indent & row & crlf);
			}
		}
	}
	return sb.toString().trim();
}


/**
Use case: You have an array of Bucket objects.
Each Bucket object has a getMemento() function that returns a struct of internal values.
By passing the array through objsToMementos() you can dump the internals for visual inspection, structFindValue(), etc.
*/
public array function objsToMementos(
	required array arrOfObjs
) {
	var out = [];
	for(var obj in arguments.arrOfObjs) {
		if(isObject(obj) && arrayLen(structFindValue(getMetaData(obj), 'getMemento', "all")) ) {
			arrayAppend(out, obj.getMemento());
		}
		else {
			arrayAppend(out, obj);
		}
	}
	return out;
}

/**
I compare 2 arrays
	compareArrays(['Jim','Dwight','Kevin'],['Michael','Dwight','Pam']) :
	{
		a_not_in_b: ['Jim','Kevin']
		, b_not_in_a: ['Michael','Pam']
		, matched: ['Dwight']
	}
*/
public struct function compareArrays(
	required array arrA
	, required array arrB
	, string key_a
	, string key_b
) {
	ret = {
		a_not_in_b: []
		, b_not_in_a: []
		, matched: []
	};
	for(var a in arguments.arrA) {
		if(!arrayContains(arguments.arrB, a)) {
			arrayAppend(ret.a_not_in_b, a);
		}
		else {
			if(!arrayContains(ret.matched, a)) {
				arrayAppend(ret.matched, a);
			}
		}
	}
	for(var b in arguments.arrB) {
		if(!arrayContains(arguments.arrA, b)) {
			arrayAppend(ret.b_not_in_a, b);
		}
	}

	arrayLen(ret.a_not_in_b) ? 	arraySort(ret.a_not_in_b, "textnocase" ) : '';
	arrayLen(ret.b_not_in_a) ? 	arraySort(ret.b_not_in_a, "textnocase" ) : '';
	arrayLen(ret.matched) ? 	arraySort(ret.matched, "textnocase" ) : '';

	if(		structKeyExists(arguments, 'key_a') && len(arguments.key_a) // && isValid("variablename",arguments.key_a)
		&& 	structKeyExists(arguments, 'key_b') && len(arguments.key_b) // && isValid("variablename",arguments.key_a)
		) {
		return {
			'matched': ret.matched
			, '#arguments.key_a#_not_in_#arguments.key_b#': ret.a_not_in_b
			, '#arguments.key_b#_not_in_#arguments.key_a#': ret.b_not_in_a
		};
	}
	else {
		return ret;
	}
}


/**
I convert a date to a string ready for pasting into roboMongo
ISODate("2015-11-10T05:00:00.000Z");
*/
public string function toRoboMongoISODate(
	required date mDate
) {
	var d = dateFormat(arguments.mDate, 'YYYY-MM-DD');
	return 'ISODate("#d#T05:00:00.000Z")';

}


/**
I take a numeric value and attempt to format it into an ssn.
	ex: 001020003 stored numerically is 1020003
	I convert it into "001-002-0003"
*/
public string function ssnNumericToFormatted(required string ssn) {
	var tmpSSN = trim(arguments.ssn);

	//if(!isNumeric(tmpSSN)) {throw('#arguments.ssn# is not numeric, BUT, it got through the NUMERIC argument constraint?');}

	if(isValid("ssn",tmpSSN )) {
		return tmpSSN;
	}
	var tmpSSN2 = '';
	// edge case exists where an ssn starting with a zero is being omitted.
	// if only 8 numbers exist, prepend a zero ... if 7 numbers, prepend two zeros
	if(reFind('(\b)(\d{8})(\b)', tmpSSN)) {
		tmpSSN = rereplace(tmpSSN, '(\b)(\d{8})(\b)', '0\2', 'one');
	}
	else if(reFind('(\b)(\d{7})(\b)', tmpSSN)) {
		tmpSSN = rereplace(tmpSSN, '(\b)(\d{7})(\b)', '00\2', 'one');
	}

	// if nine digits, insert dashes
	if(reFind('\d{9}', tmpSSN)) {
		tmpSSN2 = mid(tmpSSN,1,3) & '-' & mid(tmpSSN,4,2) & '-' & mid(tmpSSN,6,4);
	}
	return tmpSSN2;
}

/**
I return the 9 digits of a SSN without the dash (-)
*/
public string function ssnRemoveDashes(required string ssn) {
	var ret = trim(replace(arguments.ssn, '-', '', 'all'));
	return this.padTo(ret,9);
}

/**
I convert a date to an EST dateTime for mongo
*/
public any function toMongoDateTime(
	required date mVal // some kind of date
) {
	return createDate(year(arguments.mVal),month(arguments.mVal),day(arguments.mVal)); //,'Etc/GMT+5');
}

/**
I convert a mongoStruct to a CF struct in a quick and dirty manner
	- Refer to /test/RaysIntrospecter.introspectStruct() for how to expand
*/
public struct function mongoToCFStruct(required struct mStruct) {
	var s={};
	structAppend(s, arguments.mStruct, true);
	return s;
}

	public boolean function serverHasCache(required string cacheName){
		return arrayContainsNoCase(getPageContext().getConfig().getCacheConnections().keySet().toArray(), arguments.cacheName);
	}

/**
I convert a query to csv
*/
public string function queryToCSV(
	required query q
) {
	var crlf = chr(10)&chr(13);
	var out = arguments.q.columnList;
	var colArray=listToArray(out);

	for(var i=1;i<=arguments.q.recordcount;i++) {
			out &= crlf;
			var counter=1;
			for(var col in colArray) {
				out &= cleanStringForCSV(arguments.q[col][i]);
				if(counter<arrayLen(colArray)) {
					out &= ',';
					counter++;
				}
			}

		}
	return out;
}

/**
I convert a query to an array of structs.
NOTE: I require existence of queryRowToStruct()
*/
	public array function queryToArray(
		required query q
	) {
		var dataArray = [];
		var cols = ListToArray(arguments.q.columnList);
		for(var i=1;i<=arguments.q.recordCount;i++) {
			arrayAppend(dataArray, queryRowToStruct(arguments.q, i, cols));
		}
		return dataArray;
	}

/**
I convert a queryRow to a struct
*/
	public struct function queryRowToStruct(
		  required query q
		, required numeric rowNumber
		, array cols = ListToArray(arguments.q.columnList)
		) {
        var rowData = {};
        for (var i = 1; i <= ArrayLen(arguments.cols); i++) {
            rowData[arguments.cols[i]] = arguments.q[arguments.cols[i]][arguments.rowNumber];
        }
        return rowData;
    }

/**
I round a number to the nearest 2 digit decimal place. (.00)
	ex: roundify(123.4567): 123.46
*/
	public numeric function roundify(
		required numeric amt
	) {
		// expecting a number like 123.4567
		if(reFind('\.\d{3,}\b', arguments.amt)) {
			return round(arguments.amt * 100)/100;
		}
		else {
			return arguments.amt;
		}
	}

/**
I pad a number with leading zeros
	ex: padTo(678,8): 00000678
*/
	public string function padTo(
		required string myNum			//  678
		, required numeric targetLength // 	8
	) {
		var myString = toString(trim(arguments.myNum));
		return (arguments.targetLength>len(myString)) ? repeatString('0', arguments.targetLength - len(myString)) & myString : myString;
	}

/**
I return a string w/out commas, quotes, periods, or chr(10)chr(13)
*/
	public string function cleanStringForCSV(required string s) {
		var crlf = chr(10) & chr(13);
		var ret = arguments.s;
		if(len(ret)) {
			ret = replace(ret, ',', '', 'all');
			ret = replace(ret, crlf, '', 'all');
			ret = replace(ret, "'", '', 'all');
			ret = replace(ret, '"', '', 'all');
			ret = replace(ret, "&"&chr(35)&"39;", '', 'all');
			//ret = replace(ret, '.', '', 'all');
		}
		return ret;
	}

/**
 sort an array of structs by a struct.date val.
*/
public array function sortArrayOfStructsByDate(
	required array arrayToBeSearched
	, required string dateKey
	, string sortDir = 'asc' // asc or desc
) {
	var sortedArray = [];
	while(arrayLen(arguments.arrayToBeSearched)) {
		if(arguments.sortDir=='asc') {
			var tmp = this.getMinDateFromArrayOfStructs(arguments.arrayToBeSearched,arguments.dateKey);
		}
		else {
			var tmp = this.getMaxDateFromArrayOfStructs(arguments.arrayToBeSearched,arguments.dateKey);
		}
		arrayAppend(sortedArray, tmp);
		arrayDelete(arguments.arrayToBeSearched, tmp);
	}
	return sortedArray;
}



/**
 ex: someArray = [{'day':'Tuesday','somethingHappenedAt':'2014-03-11T05:00:00.000'},{'day':'Wednesday','somethingHappenedAt':'2014-03-12T05:00:00.000'}];
 result = getMaxDateFromArrayOfStructs(someArray,'somethingHappenedAt');

 	should return: {'day':'Wednesday','somethingHappenedAt':'2014-03-12T05:00:00.000'}
*/
	public struct function getMaxDateFromArrayOfStructs(
		required array arrayToBeSearched
		, required string dateKey
		, boolean useMax=true // if true we get the max date, otherwise, well return the min date
		) {
		//	maxDate from (obj.prop) in an array
		// 	get the array to search through
		//	var divisionRules = group['divisionRules'];
		// 	serialTimeArray holds the (serialized) date/times
		var serialTimeArray = [];

		if(!arrayLen(arguments.arrayToBeSearched)) {
			throw('zero length array passed to wc.utils.batman.getMaxDateFromArrayOfStructs()');
		}
		if(!len(trim(arguments.dateKey))) {
			throw('empty string cannot be used as a key in a struct. wc.utils.batman.getMaxDateFromArrayOfStructs()');
		}

		// loop over the array
		for(var item IN arguments.arrayToBeSearched) {
			// convert the date to a serialDateTime by multiplying by 1
			//    note:    divisionRule['openEnrollmentEndDate'] is a Java date obj. Conv to CF for serial date/time
			// add to the array

				arrayAppend(
					serialTimeArray,
					parseDateTime( // the val should be something like: 2014-03-11T05:00:00.000
						item[arguments.dateKey].toString()
						) * 1
					);
		}

		// serialDateTime converts time to a number.
		// using the combination of arrayMax() and arrayFind() we get the position of the max date/time in the divisionRules array
		if(arguments.useMax) {	// get the max date
				var positionInArray = arrayFind(
					serialTimeArray, arrayMax(serialTimeArray)
					);
		}
		else { // get the min date
				var positionInArray = arrayFind(
					serialTimeArray, arrayMin(serialTimeArray)
					);
		}

		// get your maxDate value
				// application.mongoUtils.toCF() ?
		return arguments.arrayToBeSearched[positionInArray];
	}

/**
Convenience opposite function of getMaxDateFromArrayOfStructs()
*/
	public struct function getMinDateFromArrayOfStructs(
		required array arrayToBeSearched
		, required string dateKey
		) {
		arguments.useMax = false;
		return getMaxDateFromArrayOfStructs(argumentCollection = arguments);
	}

/**
Convenience function to determine environment
*/
	public boolean function isWindows() {
		return FindNoCase('Win', server.os.name);
	}

}