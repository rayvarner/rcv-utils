/**
 * Logger
 * I am a simple replacement for WriteLog() that can be substituted or extended 
 * as needed. 
 *
REGEX for replacing writeLog() with Logger.logMe()
	(writeLog\(text\=)(.*?)(,\s?type\=)(.*?)(,.*?;)
	variables.Logger.logMe($2, $4);

 TIP:
    // CFC version
	param name="variables._thisFile" default="#getComponentMetaData(THIS).name#";
	
    // CFM version
    param name="variables._thisFile" default="#CGI.SCRIPT_NAME#";
    
    Logger.logMe("== #variables._thisFile#.someFunctionName() === Im an info message", "info");
 **/
 component accessors=true output=false persistent=false {

	variables.lockname = 'logger_' & Hash( GetCurrentTemplatePath() );

	public rcv.utils.Logger function init(
		string logFileName = 'dev'
	) {
		structAppend(variables, arguments, true);
		return this;
	}

    /**
     * logMe
     *
     * @mText The string to be logged
     * @mType info|warning|error|fatal
     * @logFileName name of the log file to log to
     * 
     * logMe("== getComponentMetaData(THIS).name.buildElements() === Some meaningful text", 'info|warning|error|fatal')
     */
	public any function logMe(
		required string mText		//string to log
		, string mType="info"	//info|warning|error|fatal
		, logFileName = variables.logFileName
	) {
		try {
			lock name="#variables.lockname#" type="exclusive" timeout="60" {
				writeLog(text="#arguments.mText#", type="#arguments.mType#", file="#arguments.logFileName#");
			}
		}
		catch(any e) {
			lock name="#variables.lockname#" type="exclusive" timeout="60" {
				writeLog(text="#e.message# #e.detail#", type="fatal");
			}
		}
	}

}