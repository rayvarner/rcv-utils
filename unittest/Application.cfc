﻿component {

	this.name = "rcvutils_testing_" & hash(getCurrentTemplatePath());
	this.applicationTimeout = createTimeSpan(0,0,2,0);
	this.sessionManagement = true;
	this.sessionTimeout = createTimeSpan(0,0,0,30);
	this.setClientCookies = false;
	this.mappings['/mxunit'] = expandPath('/mxunit');

	public any function onApplicationStart() {
		application['started']=now();
		writeLog(text='=========================================================', type='info', file='dev');
		writeLog(text='== onApplicationStart() #application.started# #this.name#', type='info', file='dev');
		return true;
	}

	public any function onApplicationEnd(struct applicationScope) {
		var started=arguments.applicationScope['started'];
		var appLength=TimeFormat(Now() - started, 'H:mm:ss');
		writeLog(text='== onApplicationEnd() Duration:#appLength#', type='info', file='dev');
	}

	public any function onRequestStart() {
		var headerTime = GetHttpTimeString(now());
		header name="Cache-Control" value="must-revalidate";
		header name="Pragma" value="no-cache";
		header name="Expires" value="#headerTime#";
		header name="Last-Modified" value="#headerTime#";

		objectcache action="clear";
		// clear the template cache
		pagePoolClear();
		// clear the component path cache
		componentCacheClear();
		// clear custom tag cache
		ctCacheClear();
	}

}