<cfsetting requesttimeout="5000" />
<cfscript>
args={
	component='mxunit.runner.DirectoryTestSuite'
	, method='run'
	, componentPath='rcv.unittest'
	, directory = expandPath("/rcv/unittest/")
	, recurse = true
	, returnVariable = "results"
};
</cfscript>
<cfinvoke attributeCollection="#args#" />
<cfoutput>#results.getResultsOutput('html')#</cfoutput>