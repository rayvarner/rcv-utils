﻿component {

	this.name='RCV_Utils_' & hash(getCurrentTemplatePath());
	this.applicationtimeout = createTimeSpan(0,0,10,0);
	this.sessionmanagement = false;

	public any function onError(exception, eventname) {
		writeDump(arguments);
		abort;
	}

}